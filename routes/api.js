const router = require('express').Router();
const auth = require('../controllers/authController');
const restrict = require('../middlewares/restrict');

router.get('/users', auth.getUsers);
router.get('/user/:id', restrict, auth.getUserById);
router.put('/user/:id', restrict, auth.updateUser);
router.post('/register', auth.registerApi);
router.post('/login', auth.loginApi);
router.post('/logout', auth.logoutApi);
router.put('/add-score/:id', restrict, auth.addScore);

module.exports = router;
