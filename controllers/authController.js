const { User } = require('../models');
const passport = require('../passport');

function format(user) {
    const { id, email } = user;

    return {
        id,
        email,
        accessToken: user.generateToken()
    }
}

module.exports = {
    getUsers: async (req, res) => {
        try {
            const users = await User.findAll({
                attributes: ['id', 'username', 'email', 'total_score', 'bio', 'city', 'social_media_url']
            });

            res.json(users);
        } catch (error) {
            console.log(error.message);
        }
    },
    getUserById: async (req, res) => {
        const id = req.params.id;

        try {
            const user = await User.findByPk(id);

            res.json(user);
        } catch (error) {
            console.log(error.message);
        }
    },
    updateUser: async (req, res) => {
        const id = req.params.id;

        User.updateUser(id, req.body)
        .then(response => {
            res.status(200).json({
                'status': 'success',
                'message': 'Berhasil update profile',
                'data': response
            });
        })
        .catch(err => {
            res.status(500).json({
                'status': 'error',
                'message': err
            });
        })
    },
    registerApi: (req, res, next) => {
        User.register(req.body)
        .then(user => {
            res.json({
                'status': 'success',
                'message': 'Register berhasil, silahkan login',
                'data': user
            }, 200);
        })
        .catch((err) => {
            res.json({
                'status': 'error',
                'message': err,
            }, 500);
        });
    },
    loginApi: (req, res) => {
        User.authenticate(req.body)
        .then(user => {
            res.json({
                'status': 'success',
                'message': 'Anda berhasil login',
                'data': format(user)
            }, 200);
        })
        .catch(err => {
            res.json({
                'status': 'error',
                'message': err
            }, 500);
        });
    },
    logoutApi: async (req, res, next) => {
        req.logout(function (err) {
            if (err) return next(err);
            res.json({
                'status': 'success',
                'message': 'Anda berhasil logout'
            }, 200);
        });
    },
    addScore: async (req, res) => {
        // console.log(req);
        const id = req.params.id;

        User.addScore(id, req.body)
        .then(response => {
            res.status(200).json({
                'status': 'success',
                'message': 'Berhasil update score',
                'data': response
            });
        })
        .catch(err => {
            res.status(500).json({
                'status': 'error',
                'message': err
            });
        })
    }
}