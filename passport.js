const passport = require('passport');
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');
const LocalStrategy = require('passport-local').Strategy;
const { User } = require('./models');

const options = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: 'mastergame12345'
}

async function authenticate(email, password, done) {
    try {
        const user = await User.authenticate({ username, password })

        return done(null, user);
    } catch (error) {
        
        return done(null, false, { message: error.message });
    }
}

passport.use(new JwtStrategy(options, async (payload, done) => {
    User.findByPk(payload.id)
    .then(user => done(null, user))
    .catch(err => done(err, false))
}));

passport.use(
    new LocalStrategy({ usernameField: 'username', passwordField: 'password' }, authenticate)
);

passport.serializeUser((user, done) => done(null, user.id));
passport.deserializeUser(async (id, done) => done(null, await User.findByPk(id)));

module.exports = passport;
